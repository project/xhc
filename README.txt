CONTENTS OF THIS FILE
---------------------
* Introduction
* Installation
* Configuration
* Cache Generation
* Cache Purging
* Drush commands
* Cache Refresh
* Maintainers

INTRODUCTION
------------
XML sitemap Http Cache (drush en -y xhc).

### Cache generation (url: xhc/cache_sitemap) ###
This module uses non blocking batch jobs requests for generating http cache pages for Nginx fastcgi and such, from sitemap.xml files provided by the xmlsitemap module.
The method used for cache generation is PHP's Curl "fresh connect" requests.

### Cache purging (url: xhc/purge | xhc/purge/%) ###
It can also purge all cache entries or token based single entries, given any custom purging method you provide in xhc_purge_all and xhc_purge_token hooks (see example below).

INSTALLATION
------------
The only prerequisiste is the xmlsitemap module enabled with one or more sitemap.xml files.
XHC supports multilingual websites and custom xmlsitemap_base_url paths, it will find them automatically.

CONFIGURATION
-------------
No configuration is required.

CACHE GENERATION
----------------
Just visit the xhc/cache_sitemap url with the 'administer nodes' permission, it will find the sitemap.xml file corresponding to the current language.
After visiting xhc/cache_sitemap, your will be redirected to the progress bar from a Drupal batch job.

You can issue the cache generation manually with the xhc_generate($lang) function, where $lang is an optional argument specifiying the sitemap.xml file language token from your locate-url i18n path.

CACHE PURGING
-------------
Implement xhc_purge_all or xhc_purge_token hooks to use the xhc/purge and xhc/purge/% links, respectively.

Here is an example implementation calling a /scripts/fastcgi-purge.sh purging script file in the webroot:

```
function custom_module_xhc_purge_path($token) {
  dpm(shell_exec($_SERVER['DOCUMENT_ROOT'] . '/scripts/fastcgi-purge.sh ' . $token . ''));
}

function custom_module_xhc_purge_all() {
  global $language;
  custom_module_xhc_purge_path($language->language);
}
```

The fastcgi-purge.sh script can be as simple as this (be careful with it, no prompt confirmation):
```
#!/bin/bash
#/scripts/fastcgi-purge.sh
if [ "$1" ]; then
  grep -ril --text KEY.*$1 /var/cache/nginx/cache | xargs -I% rm -v %
fi
```

DRUSH COMMANDS
--------------
Call drush xhccss (xhc-cache-sitemap) is like going to /xhc/cache_sitemap but with drush batch background processes. It will trigger xhc_generate().
You can call drush xhc-cache-sitemap with a sitemap language token as follow:
```
drush xhccs en
drush xhccs fr
```

The same goes for cache purging, use one of the following (shortcuts for drush xhc-purge-all, drush xhc-purge-token t):
```
drush xhcpa
drush xhcpt website/token
```

CACHE REFRESH
-------------
Successively visit /xhc/purge and /xhc/cache_sitemap, or call xhc_purge_all() and xhc_generate(), or use drush commands listed above (useful in a Unix cron).

MAINTAINERS
-----------
The module is maintained by B2F (ifzenelse.net)
