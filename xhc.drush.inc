<?php

function xhc_drush_command() {

  $items = array();
  $items['xhc-cache-sitemap'] = array(
    'description' => 'Cache pages from xmlsitemap files',
    'arguments' => array(
      'lang' => 'The sitemap language token used by the i18n locate-url path',
    ),
    'aliases' => array('xhccs'),
  );
  $items['xhc-purge-token'] = array(
    'description' => 'Purge the argument from the http cache',
    'arguments' => array(
      'path' => 'Path to purge',
    ),
    'aliases' => array('xhcpt')
  );
  $items['xhc-purge-all'] = array(
    'description' => 'Purge all the http cache',
    'aliases' => array('xhcpa')
  );
  return $items;

}

function drush_xhc_cache_sitemap($lang = NULL) {
  xhc_generate($lang, TRUE);
}

function drush_xhc_purge_token($token) {
  xhc_purge_token($token);
}

function drush_xhc_purge_all() {
  xhc_purge_all();
}