<?php

/**
 * @file XML sitemap http cache module.
 */

/**
 * Implements hook_menu();
 * @return array $items
 */
function xhc_menu() {
  $items['xhc/cache_sitemap'] = array(
    'page callback' => 'xhc_generate',
    'access arguments' => array('administer nodes'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['xhc/purge'] = array(
    'page callback' => 'xhc_purge_all',
    'access arguments' => array('administer nodes'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['xhc/purge/%'] = array(
    'page callback' => 'xhc_purge_token',
    'page arguments' => array(2),
    'access arguments' => array('administer nodes'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Generate the http cache from the current language's xmlsitmap.
 *
 * @global type $base_url
 * @global type $language
 * @param type $lang
 *  The url language token can be forced with this parameter.
 *  Current language will be used by default.
 */
function xhc_generate($lang = NULL, $drush = FALSE) {

  global $base_url;
  $sitemap_dir = variable_get('xmlsitemap_base_url');
  if (!isset($sitemap_dir)) {
    $sitemap_dir = $base_url;
  }

  $batch = array(
    'operations' => array(),
    'title' => t('Processing cache update from sitemap'),
    'init_message' => t('Cache update is starting.'),
    'progress_message' => t('Processed @current pages out of @total.'),
    'error_message' => t('Batch cache update has encountered an error.'),
  );

  $language_negotiation = variable_get('language_negotiation_language');
  if ($lang != NULL) {
    $sitemap_dir .= '/' . $lang;
  }
  else if (isset($language_negotiation['locale-url'])) {
    global $language;
    $sitemap_dir .= '/' . $language->language;
  }

  $sitemap_path = $sitemap_dir . '/sitemap.xml';
  if (!file_get_contents($sitemap_path)) {
    throw new Exception('Sitemap ' . $sitemap_path . ' not found.');
  }
  $sitemap = simplexml_load_file($sitemap_path);

  $urls = array();
  foreach ($sitemap->url as $url) {
    $batch['operations'][] = array('xhc_path', array($url->loc->__toString()));
  }

  batch_set($batch);

  if ($drush && !module_exists('background_batch')) {
    drush_print('Please install the background_batch module to use the drush xhc-cache-sitemap command.');
  }
  else if ($drush) {
    $batch =& batch_get();
    $batch['progressive'] = FALSE;
    drush_backend_batch_process();
  }
  else {
    batch_process('');
  }
}

/**
 * Cache a single page with a PHP Curl request.
 *
 * @param type $path
 * @param array $context
 */
function xhc_path($path, &$context) {
  $c = curl_init();
  curl_setopt($c, CURLOPT_URL, $path);
  curl_setopt($c, CURLOPT_FRESH_CONNECT, TRUE);
  curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
  curl_exec($c);
  curl_close($c);
  $context['message'] = 'Caching ' . $path;
}

/**
 * Implement this hook to purge all the http cache.
 * This function must be implemented to use the xhc/flush menu item.
 */
function xhc_purge_all() {
  return module_invoke_all('xhc_purge_all');
}

/**
 * Implement this hook to flush cache matching a token.
 *
 * @param type $token
 */
function xhc_purge_token($token) {
  return module_invoke_all('xhc_purge_token', $token);
}